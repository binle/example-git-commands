# example-git-common-commands

## Table of contents

[Simple git rename branch](./src/git-rename-branch.md)

[Simple git stash between branches](./src/git-stash-between-branch.md)

[Simple git reset](./src/git-reset.md)

[Simple git commit amend](./src/git-commit-amend.md)

[Simple git rebase](./src/git-rebase.md)

[Simple git sync forked repo to upstream](./src/git-sync-forked.md)
